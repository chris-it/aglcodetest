﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Entities;

namespace AglTest.Common
{
    /// <summary>
    /// This is the data common context interface
    /// </summary>
    public interface IDataContext: IDisposable
    {
        IQueryable<Person> Persons { get; }
    }
}
