﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Entities;

namespace AglTest.Common
{
    public interface IPersonRepository
    {
        /// <summary>
        /// Get pets grouped by the gender of the respective owner
        /// </summary>
        /// <param name="petType">Type of the pet to filter</param>
        /// <returns>DIctionary of pets</returns>
        IDictionary<string, List<Pet>> GetPetsGroupedByOwnerGender(PetType petType = PetType.All);
    }
}
