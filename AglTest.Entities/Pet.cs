﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AglTest.Entities
{
    public class Pet
    {
        public string Name { get; set; }
        public PetType Type { get; set; }
    }
}
