﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AglTest.Common;
using AglTest.Entities;
using Newtonsoft.Json;

namespace AglTest.JsonDataContext
{
    public class DataContext : IDataContext
    {
        private IQueryable<Person> persons;
        public IQueryable<Person> Persons
        {
            get
            {
                if(persons == null)
                {
                    persons = Task.Run(async () => await LoadPersons()).Result;
                }

                return persons;
            }
        }

        private async Task<IQueryable<Person>> LoadPersons()
        {
            var url = @"http://agl-developer-test.azurewebsites.net/people.json";

            using (var client = new HttpClient())
            using (var response = await client.GetAsync(url))
            using (var content = response.Content)
            {
                string json = await content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<List<Person>>(json);

                return result.AsQueryable();
            }

            // Further improvements:
            // 1. There should be exception handling to support cases when the remote service is unavailable.
            // 2. Since this is a azure data source, we could use the Retry pattern to handle transient errors.
            // 3. Use a configuration to get the URL.
        }

        public void Dispose()
        {
            // Any disposable logic can go here.
        }
    }
}
