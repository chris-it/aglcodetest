﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Common;
using AglTest.Entities;
using Moq;
using Xunit;
using Should;

namespace AglTest.Repository.Tests
{
    public class PersonRepositoryTests
    {
        [InlineData(PetType.Fish)]
        [InlineData(PetType.Dog)]
        [InlineData(PetType.Cat)]
        [Theory]
        public void GetPetsGroupedByOwnerGender_ShouldFilterPetsByType(PetType petType)
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = "Male",
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="P1", Type= PetType.Cat },
                        new Pet(){ Name="P2", Type= PetType.Dog },
                        new Pet(){ Name="P3", Type= PetType.Dog },
                        new Pet(){ Name="P4", Type= PetType.Fish }
                    }
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            IDictionary<string, List<Pet>> result = sut.GetPetsGroupedByOwnerGender(petType);

            ///////// Assert

            result.SelectMany(p => p.Value).All(p => p.Type == petType).ShouldBeTrue();
        }

        [Fact]
        public void GetPetsGroupedByOwnerGender_ShouldOrderPetsByNameAscending()
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = "Male",
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="Garfield", Type= PetType.Cat },
                        new Pet(){ Name="Tom", Type= PetType.Cat },
                        new Pet(){ Name="Fido", Type= PetType.Dog },
                        new Pet(){ Name="Nemo", Type= PetType.Fish }
                    }
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            IDictionary<string, List<Pet>> result = sut.GetPetsGroupedByOwnerGender();

            ///////// Assert

            var allPets = result.SelectMany(p => p.Value).ToList();
            allPets[0].Name.ShouldBeSameAs("Fido");
            allPets[1].Name.ShouldBeSameAs("Garfield");
            allPets[2].Name.ShouldBeSameAs("Nemo");
            allPets[3].Name.ShouldBeSameAs("Tom");
        }

        [Fact]
        public void GetPetsGroupedByOwnerGender_ShouldGroupOwnerGender()
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = "Male",
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="Garfield", Type= PetType.Cat },
                        new Pet(){ Name="Tom", Type= PetType.Cat },
                    }
                },
                new Person(){
                    Gender = "Female",
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="Fido", Type= PetType.Dog },
                        new Pet(){ Name="Nemo", Type= PetType.Fish }
                    }
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            IDictionary<string, List<Pet>> result = sut.GetPetsGroupedByOwnerGender();

            ///////// Assert

            var malePets = result["Male"];
            malePets.All(p => p.Name == "Garfield" || p.Name == "Tom").ShouldBeTrue();
            malePets.All(p => p.Name == "Fido" || p.Name == "Nemo");
        }

        [Fact]
        public void GetPetsGroupedByOwnerGender_FilterAll_ShouldReturnAllPets()
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = "Male",
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="P1", Type= PetType.Cat },
                        new Pet(){ Name="P2", Type= PetType.Dog },
                        new Pet(){ Name="P3", Type= PetType.Fish }
                    }
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            IDictionary<string, List<Pet>> result = sut.GetPetsGroupedByOwnerGender(PetType.All);

            ///////// Assert

            var allPets = result.SelectMany(p => p.Value);
            allPets.Any(p => p.Type == PetType.Cat).ShouldBeTrue();
            allPets.Any(p => p.Type == PetType.Dog).ShouldBeTrue();
            allPets.Any(p => p.Type == PetType.Fish).ShouldBeTrue();
        }

        [Fact]
        public void GetPetsGroupedByOwnerGender_PersonWithNullPets_ShouldReturnEmptyResult()
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = "Male",
                    Pets = null
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            IDictionary<string, List<Pet>> result = sut.GetPetsGroupedByOwnerGender();

            ///////// Assert

            result.ShouldBeEmpty();
        }

        [Fact]
        public void GetPetsGroupedByOwnerGender_PersonDataWithoutGender_ShouldThrowExeption()
        {
            ///////// Assert

            Mock<IDataContext> mockDataContext = new Mock<IDataContext>();
            // Create people data
            List<Person> people = new List<Person>()
            {
                new Person(){
                    Gender = null,
                    Pets = new List<Pet>()
                    {
                        new Pet(){ Name="P1", Type= PetType.Cat },
                        new Pet(){ Name="P2", Type= PetType.Dog },
                        new Pet(){ Name="P3", Type= PetType.Fish }
                    }
                },
            };
            mockDataContext.Setup(p => p.Persons).Returns(people.AsQueryable());

            ///////// Act and Assert

            IPersonRepository sut = new PersonRepository(mockDataContext.Object);
            Assert.Throws<ArgumentNullException>(()=> sut.GetPetsGroupedByOwnerGender());
        }
    }
}
