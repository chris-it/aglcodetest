﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Common;
using AglTest.Entities;

namespace AglTest.Repository
{
    public class PersonRepository : IPersonRepository, IDisposable
    {
        private IDataContext context;

        public PersonRepository(IDataContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get pets grouped by the gender of the respective owner
        /// </summary>
        /// <returns>Dictionary of pets</returns>
        public IDictionary<string, List<Pet>> GetPetsGroupedByOwnerGender(PetType petType = PetType.All)
        {
            var pets = from person in this.context.Persons
                       where person.Pets != null 
                       group person by person.Gender into grp
                       select new
                       {
                           Gender = grp.Key,
                           Pets = grp
                            .SelectMany(p => p.Pets)
                            .Where(p => petType == PetType.All || p.Type == petType)
                            .OrderBy(p => p.Name)
                       };

            return pets.ToDictionary(p => p.Gender, p => p.Pets.ToList());
        }

        #region Disposable
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
