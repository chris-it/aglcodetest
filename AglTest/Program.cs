﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Entities;

namespace AglTest
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHeader();

            // Initialize the runner and get the cats from the data source
            Runner runner = new Runner();
            var groupedPets = runner.GetPetsGroupedByOwnerGender(PetType.Cat);

            PrintGroupedPets(groupedPets);

            Console.WriteLine();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();

            // Further improvements:
            // 1. Could use a command line parser to handle different options such as filter Dogs, Fish, etc.
        }

        private static void PrintHeader()
        {
            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Print all cats in alphabetical order under a heading of the gender of their owner");
            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine();
        }
        private static void PrintGroupedPets(IDictionary<string, List<Pet>> groupedPets)
        {
            foreach (var group in groupedPets)
            {
                Console.WriteLine(group.Key);
                foreach (var pet in group.Value)
                {
                    Console.WriteLine(string.Format("\t- {0}", pet.Name));
                }
            }
        }
    }
}
