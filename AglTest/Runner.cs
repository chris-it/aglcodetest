﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AglTest.Common;
using AglTest.Entities;
using AglTest.JsonDataContext;
using AglTest.Repository;
using Autofac;

namespace AglTest
{
    public class Runner
    {
        private IContainer container;

        public Runner()
        {
            // Initialize the repository and data context dependencies.
            InitializeDependencies();
        }

        private IPersonRepository PersonRepository
        {
            get { return container.Resolve<IPersonRepository>(); }
        }

        /// <summary>
        /// Get pets grouped by the gender of the respective owner
        /// </summary>
        /// <param name="petType">Type of the pet to filter</param>
        /// <returns>DIctionary of pets</returns>
        public IDictionary<string, List<Pet>> GetPetsGroupedByOwnerGender(PetType petType)
        {
            try
            {
                return this.PersonRepository.GetPetsGroupedByOwnerGender(petType);
            }
            catch (Exception)
            {
                throw;

                // Further improvements:
                // 1. Custom error handling.
                // 2. Logging errors using log4net or similar.
            }
        }

        private void InitializeDependencies()
        {
            var builder = new ContainerBuilder();

            // Register all the dependencies
            builder.RegisterType<DataContext>().As<IDataContext>().SingleInstance();
            builder.RegisterType<PersonRepository>().As<IPersonRepository>().SingleInstance();

            container = builder.Build();
        }
    }
}
