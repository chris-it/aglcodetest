AGL Coding Test
===============

Summary
-------
* AglTest- This is the main C# console project. Running this project will output the result to the console.
* AglTest.Repository.Tests - This is the test project that uses xUnit (xUnit Visual Studio runner is required to run tests in VS.)
* All the other projects are related class libraries.


Additional details
------------------
* Uses Repository pattern for data access.
* Use of several 3rd party libraries such as Autofac, Newtonsoft JSON, xUnit, MOQ, Shouldly.
* This needs xUnit runner for VS to run tests.

Further improvements
--------------------
There can be lot of improvements to make this release ready and some notes are added inside respective methods as comments. And those are;

* Could use a command line parser to handle different options such as filter Dogs, Fish, etc.
* Custom error handling
* Logging errors using log4net or similar
* There should be exception handling to support cases when the remote service is unavailable
* Since this is a azure data source, we could use the Retry pattern to handle transient errors.
* Use a configuration manager to configure URLs, etc.
* Currently this only has unit testing for the repository. We could have more integration tests and end to end tests.

